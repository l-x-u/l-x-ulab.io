const path = require("path")

// https://www.gatsbyjs.com/docs/reference/config-files/gatsby-node/#createPages
exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions
  const result = await graphql(`
  query {
    allMdx {
      edges {
        node {
          id
          frontmatter {
            slug
          }
          fields {
            source
          }
        }
      }
    }
  }
  
  `)

  if (result.errors) {
    reporter.panicOnBuild('🚨  ERROR: Loading "createPages" query')
  }

  // create blog post pages
  const posts = result.data.allMdx.edges

  const blog = posts.filter(edge => edge.node.fields.source === "blog-posts")
  const portfolio = posts.filter(edge => edge.node.fields.source === "portfolio")

  // call `createPage` for each result
  blog.forEach(({ node }, index) => {
    createPage({
      path: "blog/" + node.frontmatter.slug,
      component: path.resolve(`./src/page-templates/blog-post.jsx`),
      // you can use the values in this context in
      // our page layout component
      context: { id: node.id },
    })
  })

}
