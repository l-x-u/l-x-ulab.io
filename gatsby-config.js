module.exports = {
  siteMetadata: {
    siteUrl: "https://l-x-u.space/",
    title: "Lou Thompson",
  },
  plugins: [
    "gatsby-plugin-image",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-mdx",
    "gatsby-plugin-sharp",
    "gatsby-transformer-sharp",
    "gatsby-plugin-mdx-source-name",
    {
      resolve: "gatsby-plugin-google-fonts",
      options: {
          fonts: [
              "merriweather:300,400,700"      
          ], display: 'swap'       
      }
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: "./src/pages/",
      },
      __key: "pages",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "blog-posts",
        path: "./src/blog-posts/",
      },
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "portfolio",
        path: "./src/portfolio/",
      },
    },
  ],
};
