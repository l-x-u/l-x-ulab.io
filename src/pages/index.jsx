import React from "react"
import { Col, Container, ListGroup, Row } from "react-bootstrap";
import { graphql } from 'gatsby'
import BlogBlurb from "../components/blog-blurb"

const Index = ({ data: { allMdx: {edges} } }) => <Container>
    <Row>
        <Col>
            <h1>Lou Thompson</h1>
        </Col>
        <Col>
            There's nothing here yet!
            <ListGroup variant="flush">
            {edges.map(edge => <ListGroup.Item>
              <BlogBlurb key={edge.node.id} post={edge.node}/>
            </ListGroup.Item>)}
            </ListGroup>
            
        </Col>
    </Row>
</Container>

export default Index

export const pageQuery = graphql`
query {
  allMdx(
    filter: {fields: {source: {eq: "blog-posts"}}}
    sort: {order: DESC, fields: frontmatter___date}
  ) {
    edges {
      node {
        id
        frontmatter {
          title
          slug
          author
          date
        }
      }
    }
  }
}

`