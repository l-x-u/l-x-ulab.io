import React from "react"
import { graphql } from "gatsby"
import { MDXProvider } from "@mdx-js/react"
import { MDXRenderer } from "gatsby-plugin-mdx"
import components from "../components/blog-mdx-components"

export default function PageTemplate({ data: { mdx } }) {
  return (
    <div>
    <h1>{mdx.frontmatter.title}</h1>
    <h4>{`${mdx.frontmatter.date} by ${mdx.frontmatter.author}`}</h4>
    <MDXProvider components={components}>
        <MDXRenderer>{mdx.body}</MDXRenderer>
    </MDXProvider>
    </div>
  )
}

export const pageQuery = graphql`
  query BlogPostQuery($id: String) {
    mdx(id: { eq: $id }) {
      id
      body
      frontmatter {
        title
        date
        author
      }
    }
  }
`
